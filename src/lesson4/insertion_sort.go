package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	numbers := make([]int64, 0)
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("Enter number and press enter and again... You need to enter blank line or any symbol except number to finish")

	for {
		scanner.Scan()
		number, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if nil != err {
			break
		}

		numbers = append(numbers, number)
	}

	sortedNumbers := insertionSort(numbers)

	fmt.Printf("Input: %d \n", numbers)
	fmt.Printf("Sorted: %d", sortedNumbers)
}

func insertionSort(numbers []int64) (sortedNumbers []int64) {
	sortedNumbers = make([]int64, len(numbers))
	copy(sortedNumbers, numbers)

	for i := 1; i < len(sortedNumbers); i++ {
		for j := i; j > 0 && sortedNumbers[j-1] > sortedNumbers[j]; j-- {
			buff := sortedNumbers[j]
			sortedNumbers[j] = sortedNumbers[j-1]
			sortedNumbers[j-1] = buff
		}
	}

	return
}
