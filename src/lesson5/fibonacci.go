package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var fibCache map[uint64]uint64

func main() {
	fibCache = make(map[uint64]uint64)
	var number uint64
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Print("Enter ordinal number greater or equal zero: ")
		scanner.Scan()
		input, err := strconv.ParseUint(scanner.Text(), 10, 64)
		if nil == err {
			number = input
			break
		}

		fmt.Print("Number is nor correct. ")
	}

	fmt.Println("Result is", fib(number, true))
}

func fib(number uint64, useCache bool) uint64 {
	if number == 0 {
		return 0
	} else if number == 1 {
		return 1
	}

	if useCache {
		return fibWithCache(number-1) + fibWithCache(number-2)
	}

	return fib(number-1, useCache) + fib(number-2, useCache)
}

func fibWithCache(number uint64) uint64 {
	_, cacheExists := fibCache[number]
	if !cacheExists {
		fibCache[number] = fib(number, true)
	}

	return fibCache[number]
}
