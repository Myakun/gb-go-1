package main

import (
	"fmt"
	"math"
)

func main() {
	var area int

	fmt.Print("Enter circle area: ")
	_, err := fmt.Scanf("%d\n", &area)
	if err != nil {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	diameter := 2 * math.Sqrt(float64(area)/math.Pi)
	length := math.Pi * diameter

	fmt.Printf("Circle diameter is %f \n", diameter)
	fmt.Printf("Circle length is %f", length)
}
