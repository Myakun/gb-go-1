package main

import (
	"fmt"
)

func main() {
	var number int

	for number < 100 || number > 999 {
		fmt.Print("Enter number from 100 to 999: ")
		_, err := fmt.Scanf("%d\n", &number)
		if err != nil {
			fmt.Printf("Something goes wrong: %s", err)
			return
		}
	}

	fmt.Printf("Hundreds: %d \n", number/100)
	fmt.Printf("Tens: %d \n", number%100/10)
	fmt.Printf("Ones: %d", number%10)
}
