package main

import (
	"flag"
	"fmt"
)

func main() {
	var side1Pointer = flag.Int("side1", 0, "Side 1")
	var side2Pointer = flag.Int("side2", 0, "Side 2")

	flag.Parse()

	var side1 = *side1Pointer
	var side2 = *side2Pointer

	fmt.Printf("Area of rectangle is %d", side1*side2)
}
