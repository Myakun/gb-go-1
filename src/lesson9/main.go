package main

import (
	"flag"
	"fmt"
	"gb-go-1/lesson9/config"
	"log"
	"os"
)

var (
	configType = flag.String("config-type", "env", "Config type: env json yml")
	configFile = flag.String("config-file", "", "Config file path for json or yml config type")
)

func main() {
	flag.Parse()

	appConfig := new(config.Config)
	var err error

	if "env" == *configType {
		appConfig, err = config.GetFromEnv()
	} else if "json" == *configType {
		if "" == *configFile {
			fmt.Printf("Param config-file can't be empty for json config type")
			return
		}

		appConfig, err = config.LoadFromJSONFile(*configFile)
	} else if "yml" == *configType {
		if "" == *configFile {
			fmt.Printf("Param config-file can't be empty for yml config type")
			return
		}

		appConfig, err = config.LoadFromYMLFile(*configFile)
	} else {
		fmt.Printf("Unsupported config type: %s", *configType)
		return
	}

	if nil != err {
		fmt.Printf("Can't load config from %s: %s", *configType, err)
		os.Exit(1)
	}

	// Create logger
	logPath := appConfig.VarDir + string(os.PathSeparator) + appConfig.LogFilename
	logFile, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if nil != err {
		panic(fmt.Sprintf("Can't open log file %s", err))
	}
	defer func() {
		if err := logFile.Close(); nil != err {
			panic(err)
		}
	}()
	appLog := log.New(logFile, "", log.Ldate|log.Ltime)

	appLog.Println("App initialized")

	appConfig.LogData(appLog)
}
