package config

import (
	"encoding/json"
	"fmt"
	"io"
)

func LoadFromJSON(reader io.Reader) (*Config, error) {
	cfg := Config{}

	jsonBytes, err := io.ReadAll(reader)
	if nil != err {
		return nil, err
	}

	err = json.Unmarshal(jsonBytes, &cfg)
	if nil != err {
		return nil, err
	}

	cfg.Sanitize()

	err = cfg.Validate()
	if nil != err {
		return nil, fmt.Errorf("invalid config: %s", err)
	}

	return &cfg, nil
}

func LoadFromJSONFile(filename string) (*Config, error) {
	reader, err := getConfigFileReader(filename)
	if nil != err {
		return nil, err
	}
	defer func() {
		if err := reader.Close(); nil != err {
			panic(err)
		}
	}()

	return LoadFromJSON(reader)
}
