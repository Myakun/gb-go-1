package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io"
)

func LoadFromYML(reader io.Reader) (*Config, error) {
	cfg := Config{}

	ymlBytes, err := io.ReadAll(reader)
	if nil != err {
		return nil, err
	}

	err = yaml.Unmarshal(ymlBytes, cfg)
	if nil != err {
		return nil, err
	}

	cfg.Sanitize()

	err = cfg.Validate()
	if nil != err {
		return nil, fmt.Errorf("invalid config: %s", err)
	}

	return &cfg, nil
}

func LoadFromYMLFile(filename string) (*Config, error) {
	reader, err := getConfigFileReader(filename)
	if nil != err {
		return nil, err
	}
	defer func() {
		if err := reader.Close(); nil != err {
			panic(err)
		}
	}()

	return LoadFromYML(reader)
}
