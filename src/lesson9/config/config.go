package config

import (
	"errors"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"log"
	"os"
	"reflect"
	"strings"
)

type Config struct {
	Env           string `envconfig:"ENV" json:"env" required:"true" json:"env" yaml:"env"`                 // Env type: dev, stage, prod
	LogFilename   string `envconfig:"LOG_FILENAME" required:"true" json:"log_filename" yaml:"log_filename"` // File to write app logs. Only filename without path.
	RedisDatabase int    `envconfig:"REDIS_DATABASE" required:"true" json:"redis_database" yaml:"redis_database"`
	RedisHost     string `envconfig:"REDIS_HOST" required:"true" json:"redis_host" yaml:"redis_host"`
	RedisPassword string `envconfig:"REDIS_PASSWORD" json:"redis_password" yaml:"redis_password"`
	RedisPort     int    `envconfig:"REDIS_PORT" required:"true" json:"redis_port" yaml:"redis_port"`
	VarDir        string `envconfig:"VAR_DIR" required:"true" json:"var_dir" yaml:"var_dir"` // Directory to store runtime data
}

func (cfg *Config) Sanitize() {
	cfg.VarDir = strings.TrimRight(cfg.VarDir, string(os.PathSeparator))
}

func (cfg *Config) Validate() error {
	envsAllowed := []string{"dev", "prod", "stage"}
	flag := false
	for _, env := range envsAllowed {
		if env == cfg.Env {
			flag = true
		}
	}
	if !flag {
		return fmt.Errorf("unknown envinroment %s", cfg.Env)
	}

	if "" == cfg.LogFilename {
		return errors.New("log filename is empty")
	}

	if cfg.RedisDatabase < 0 || cfg.RedisDatabase > 15 {
		return fmt.Errorf("redis database need to be from 0 to 15, not %d", cfg.RedisDatabase)
	}

	if "" == cfg.RedisHost {
		return errors.New("redis host is empty")
	}

	if cfg.RedisPort <= 0 {
		return fmt.Errorf("incorrect redis port: %d", cfg.RedisPort)
	}

	info, err := os.Stat(cfg.VarDir)

	if nil != err {
		return fmt.Errorf("var directory %s not exists", cfg.VarDir)
	}

	if !info.IsDir() {
		return fmt.Errorf("var directory %s isn't a directory", cfg.VarDir)
	}

	tmpName := cfg.VarDir + string(os.PathSeparator) + "check-var-dir-writable"
	file, err := os.Create(tmpName)
	if nil != err {
		return fmt.Errorf("var directory %s isn't writable", cfg.VarDir)
	}

	if err = file.Close(); nil != err {
		return err
	}

	if err = os.Remove(tmpName); nil != err {
		return err
	}

	return nil
}

func GetFromEnv() (*Config, error) {
	cfg := Config{}

	err := envconfig.Process("APP", cfg)
	if nil != err {
		return nil, fmt.Errorf("can't process the config: %s", err)
	}

	cfg.Sanitize()

	err = cfg.Validate()
	if nil != err {
		return nil, fmt.Errorf("invalid config: %s", err)
	}

	return &cfg, nil
}

func (cfg Config) LogData(logger *log.Logger) {
	value := reflect.ValueOf(cfg)
	typeOfValue := value.Type()
	for i := 0; i < value.NumField(); i++ {
		logger.Printf("App config %s: %v\n", typeOfValue.Field(i).Name, value.Field(i).Interface())
	}
}

func getConfigFileReader(filename string) (reader *os.File, err error) {
	info, err := os.Stat(filename)
	if nil != err {
		return nil, fmt.Errorf("config file %s not exists", filename)
	}

	if info.IsDir() {
		return nil, fmt.Errorf("config file %s is a directory", filename)
	}

	reader, err = os.Open(filename)
	if nil != err {
		return nil, err
	}

	return reader, nil
}
