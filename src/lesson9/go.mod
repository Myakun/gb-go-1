module gb-go-1/lesson9

go 1.16

require (
	github.com/kelseyhightower/envconfig v1.4.0
	gopkg.in/yaml.v2 v2.4.0
)
