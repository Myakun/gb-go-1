package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

var scanner *bufio.Scanner

func main() {
	scanner = bufio.NewScanner(os.Stdin)

	a := scanFloat(1)
	b := scanFloat(2)
	operation := scanOperation()

	var res float64

	// I prefer to use if-else then switch-case
	if "+" == operation {
		res = a + b
	} else if "-" == operation {
		res = a - b
	} else if "*" == operation {
		res = a * b
	} else if "/" == operation {
		if b == 0 {
			fmt.Println("Division by zero is prohibited")
			return
		}
		res = a / b
	} else if "pow" == operation {
		res = math.Pow(a, b)
	}

	fmt.Printf("Result of operation is %f", res)
}

func scanFloat(pos int) float64 {
	var number float64

	fmt.Printf("Enter number #%d: ", pos)
	scanner.Scan()
	number, err := strconv.ParseFloat(scanner.Text(), 10)
	if err != nil {
		number = scanFloat(pos)
	}

	return number
}

func scanOperation() (operation string) {
	operations := []string{"+", "-", "*", "/", "pow"}

	fmt.Printf("Enter one of operations %q: ", operations)
	_, err := fmt.Scanf("%s", &operation)
	if err != nil {
		fmt.Println("Something is wrong. Repeat please")
		operation = scanOperation()
	}

	for _, op := range operations {
		if op == operation {
			return
		}
	}

	fmt.Println("Operation not found")
	operation = scanOperation()

	return
}
