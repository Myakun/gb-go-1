package main

import (
	"fmt"
	"math"
)

func main() {
	var number int

	fmt.Print("Enter number: ")
	_, err := fmt.Scanf("%d\n", &number)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	if number <= 1 {
		fmt.Println("No prime numbers")
		return
	}

	fmt.Print("2 ")

	for i := 3; i <= number; i += 2 {
		isPrime := true
		for j := 3; j <= int(math.Sqrt(float64(i))); j += 2 {
			if i%j == 0 {
				isPrime = false
				break
			}
		}

		if isPrime {
			fmt.Printf("%d ", i)
		}
	}
}
