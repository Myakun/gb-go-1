package main

import (
	"fmt"
	"gb-go-1/lesson8/config"
	"log"
	"os"
)

func main() {
	appConfig, err := config.GetFromEnv()
	if nil != err {
		fmt.Printf("Can't load config from ENV: %s", err)
		os.Exit(1)
	}

	// Create logger
	logPath := appConfig.VarDir + string(os.PathSeparator) + appConfig.LogFilename
	logFile, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if nil != err {
		panic(fmt.Sprintf("Can't open log file %s", err))
	}
	defer func() {
		if err := logFile.Close(); nil != err {
			panic(err)
		}
	}()
	appLog := log.New(logFile, "", log.Ldate|log.Ltime)

	appLog.Println("App initialized")

	appConfig.LogData(appLog)
}
