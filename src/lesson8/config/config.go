package config

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"log"
	"os"
	"reflect"
	"strings"
)

type Config struct {
	Env           string `envconfig:"ENV" required:"true"`          // Env type: dev, stage, prod
	LogFilename   string `envconfig:"LOG_FILENAME" required:"true"` // File to write app logs. Only filename without path
	RedisDatabase int    `envconfig:"REDIS_DATABASE" default:"0"`
	RedisHost     string `envconfig:"REDIS_HOST" default:"localhost"`
	RedisPassword string `envconfig:"REDIS_PASSWORD"`
	RedisPort     int    `envconfig:"REDIS_PORT" default:"6379"`
	VarDir        string `envconfig:"VAR_DIR" required:"true"` // Directory to store runtime data
}

func sanitizeConfig(cfg *Config) {
	cfg.VarDir = strings.TrimRight(cfg.VarDir, string(os.PathSeparator))
}

func GetFromEnv() (Config, error) {
	cfg := Config{}

	err := envconfig.Process("APP", &cfg)
	if nil != err {
		return cfg, fmt.Errorf("can't process the config: %s", err)
	}

	sanitizeConfig(&cfg)

	err = validateConfig(&cfg)
	if nil != err {
		return cfg, fmt.Errorf("invalid config: %s", err)
	}

	return cfg, nil
}

func (cfg Config) LogData(logger *log.Logger) {
	value := reflect.ValueOf(cfg)
	typeOfValue := value.Type()
	for i := 0; i < value.NumField(); i++ {
		logger.Printf("App config %s: %v\n", typeOfValue.Field(i).Name, value.Field(i).Interface())
	}
}

func validateConfig(cfg *Config) error {
	envsAllowed := []string{"dev", "prod", "stage"}
	flag := false
	for _, env := range envsAllowed {
		if env == cfg.Env {
			flag = true
		}
	}
	if !flag {
		return fmt.Errorf("unknown envinroment %s", cfg.Env)
	}

	if cfg.RedisDatabase < 0 || cfg.RedisDatabase > 15 {
		return fmt.Errorf("redis database need to be from 0 to 15, not %d", cfg.RedisDatabase)
	}

	info, err := os.Stat(cfg.VarDir)

	if nil != err {
		return fmt.Errorf("var directory %s not exists", cfg.VarDir)
	}

	if !info.IsDir() {
		return fmt.Errorf("var directory %s isn't a directory", cfg.VarDir)
	}

	tmpName := cfg.VarDir + string(os.PathSeparator) + "check-var-dir-writable"
	file, err := os.Create(tmpName)
	if nil != err {
		return fmt.Errorf("var directory %s isn't writable", cfg.VarDir)
	}

	if err = file.Close(); nil != err {
		return err
	}

	if err = os.Remove(tmpName); nil != err {
		return err
	}

	return nil
}
