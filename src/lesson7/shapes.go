package main

import (
	"fmt"
	"gb-go-1/lesson7/shape"
	"os"
)

func main() {
	// Enter disk area
	var area float64
	fmt.Print("Enter disk area: ")
	_, err := fmt.Scanf("%f\n", &area)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	// Print disk data
	disk := shape.NewDisk(area)
	err = shape.PrintCircleData(disk, os.Stdout)
	if nil != err {
		fmt.Println(err)
	}

	// Enter square data
	var side float64
	fmt.Print("Enter square side: ")
	_, err = fmt.Scanf("%f\n", &side)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	// Print square data
	square := shape.NewSquare(side)
	err = shape.PrintShapeData(square, os.Stdout)
	if nil != err {
		fmt.Println(err)
	}

	// Enter triangle data
	var sideA, sideB, angle float64

	fmt.Print("Enter triangle side A: ")
	_, err = fmt.Scanf("%f\n", &sideA)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	fmt.Print("Enter triangle side B: ")
	_, err = fmt.Scanf("%f\n", &sideB)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	fmt.Print("Enter triangle angle between sides A and B: ")
	_, err = fmt.Scanf("%f\n", &angle)
	if nil != err {
		fmt.Printf("Something goes wrong: %s", err)
		return
	}

	// Print triangle data
	triangle := shape.NewTriangle(sideA, sideB, angle)
	err = shape.PrintShapeData(triangle, os.Stdout)
	if nil != err {
		fmt.Println(err)
	}
}
