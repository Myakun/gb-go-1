package shape

import "math"

type Triangle struct {
	Shape
	sideA float64
	sideB float64
	angle float64
}

func (t Triangle) Area() float64 {
	return 0.5 * t.sideA * t.sideB * math.Sin(t.angle)
}

func (t Triangle) Length() float64 {
	return math.Sqrt(math.Pow(t.sideA, 2)+math.Pow(t.sideB, 2)-2*t.sideA*t.sideB*math.Cos(t.angle)) + t.sideA + t.sideB
}

func (t Triangle) Name() string {
	return "Triangle"
}

func NewTriangle(sideA, sideB, angle float64) *Triangle {
	return &Triangle{angle: angle, sideA: sideA, sideB: sideB}
}
