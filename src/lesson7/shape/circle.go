package shape

import (
	"fmt"
	"io"
	"strings"
)

type Circle interface {
	Shape
	Diameter() float64
}

func PrintCircleData(circle Circle, writer io.Writer) error {
	err := PrintShapeData(circle, writer)
	if nil != err {
		return err
	}

	_, err = fmt.Fprintf(writer, "Diameter of %s: %f \n", strings.ToLower(circle.Name()), circle.Diameter())
	if nil != err {
		return ShapePrintError{method: "Diameter", originalError: err}
	}

	return nil
}
