package shape

import "math"

type Disk struct {
	area float64
}

func (d Disk) Area() float64 {
	return d.area
}

func (d Disk) Diameter() float64 {
	return 2 * math.Sqrt(d.area/math.Pi)
}

func (d Disk) Length() float64 {
	return math.Pi * d.Diameter()
}

func (d Disk) Name() string {
	return "Disk"
}

func NewDisk(area float64) *Disk {
	return &Disk{area: area}
}
