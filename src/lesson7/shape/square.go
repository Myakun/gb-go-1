package shape

type Square struct {
	Shape
	side float64
}

func (s Square) Area() float64 {
	return s.side * s.side
}

func (s Square) Length() float64 {
	return s.side * 4
}

func (s Square) Name() string {
	return "Square"
}

func NewSquare(side float64) *Square {
	return &Square{side: side}
}
