package shape

import (
	"fmt"
	"io"
	"strings"
)

type Shape interface {
	Area() float64
	Length() float64
	Name() string
}

type ShapePrintError struct {
	method        string
	originalError error
}

func (err ShapePrintError) Error() string {
	return fmt.Sprintf("%s of shape write error: %s", err.method, err.originalError.Error())
}

func PrintShapeData(shape Shape, writer io.Writer) error {
	_, err := fmt.Fprintf(writer, "Area of %s: %f \n", strings.ToLower(shape.Name()), shape.Area())
	if nil != err {
		return ShapePrintError{method: "Area", originalError: err}
	}

	_, err = fmt.Fprintf(writer, "Length of shape %s: %f \n", strings.ToLower(shape.Name()), shape.Length())
	if nil != err {
		return ShapePrintError{method: "Length", originalError: err}
	}

	return nil
}
